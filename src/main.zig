const std = @import("std");  const z = @import("glfw");  const g = @import("gpu");
pub const cg = @cImport({
    @cInclude("dawn/webgpu.h"); @cInclude("dawn/dawn_proc.h"); @cInclude("dawn_native_mach.h");
});
const k = @cImport({@cInclude("stddef.h"); @cInclude("k.h");});

const prt = std.debug.print;  const zs = std.mem.zeroes;
const title = "K Entertaiment System";

// 480×270 32 colors

fn add(x: k.K, y: k.K) k.K {const a=k.iK(x);  const b=k.iK(y);  return k.Ki(a+b);}

fn test_k() void {
    k.kinit();  // k.KR("add", @ptrCast(*const anyopaque, add), 2);

    var f: k.K = 0;  _ = k.K0(&f, "add:{x+y}", null, 0);
    const l = k.Ki(2);  const r = k.Ki(40);
    f = 0;  const res = k.K0(&f, "add", &[2]k.K{l,r}, 2);
    prt("{} {} {}", .{res, f, k.iK(res)});
}

//  setbuf(stdout,0);
//  printf("kinit()\n"); kinit();
//  printf("KR()\n");    KR("add",add,2);/
//  printf("KC()\n");    K s=KC("`0:$add[2;3]",13);
//  printf("K1()\n");    K1('.',s);
//  printf("return\n");  return 0;
// #define Kx(s,a...) ({ static K f; K0(&f, s, (K[]){a}, sizeof((K[]){a})/sizeof(K)); })

const WD = struct {
    surf: ?g.Surface,  sc: ?g.SwapChain,  scf: g.Texture.Format,
    pws: [2]u32,  nws: [2]u32,
};

fn b2i(b: bool) u8 {return @boolToInt(b);}
fn tr(i: anytype) u8 {return @truncate(u8, @intCast(c_uint, std.math.max(0,i)));}

pub fn main() anyerror!void {
    prt("GLFW {}.{}.{}\n", .{z.version.major, z.version.minor, z.version.revision});

    k.kinit();  const fw = @embedFile("fw.k");  {var f: k.K = 0;  _ = k.K0(&f, fw, null, 0);}

    try z.init(.{});  prt("GLFW Init Succeeded.\n", .{});
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};  var al = gpa.allocator();

    var win = try z.Window.create(960, 540, title, null, null, .{.client_api=.no_api});
    const wd = try al.create(WD);
    wd.* = .{.surf=null, .sc=null, .scf=undefined, .pws = .{0,0}, .nws = .{0,0}};
    var set = try setWgpu(&win, wd);
    const pl = setPipeline(set.dev, wd);

    const is = g.Extent3D {.width=480, .height=270};
    const tx = set.dev.createTexture(&.{
        .size = is,  .format = .rgba8_unorm,
        .usage = .{.texture_binding=true, .copy_dst=true, .render_attachment=true},
    });
    
    const sm = set.dev.createSampler(&.{.mag_filter=.nearest, .min_filter=.nearest});
    const bg = set.dev.createBindGroup(&g.BindGroup.Descriptor {
        .layout = pl.getBindGroupLayout(0),
        .entries = &.{
            g.BindGroup.Entry.sampler(0, sm),
            g.BindGroup.Entry.textureView(1, tx.createView(&g.TextureView.Descriptor{})),
        },
    });

    var px = zs([480*270][4]u8);
    for(px) |*v,i| {v.* = .{127*b2i(i%7==0), 0, 0, 1};}
    const dl = g.Texture.DataLayout{.bytes_per_row=4*480, .rows_per_image=270};

    const qu = set.dev.getQueue();
    {const pxs: []const [4]u8 = px[0..];  qu.writeTexture(&.{.texture=tx}, pxs, &dl, &is);}

    win.setFramebufferSizeCallback((struct {
        fn cb(window: z.Window, w: u32, h: u32) void {
            const p = window.getUserPointer(WD);  p.?.nws = .{w, h};
        }
    }).cb);

    win.setKeyCallback((struct {
        fn cb(_: z.Window, _: z.Key, s: i32, a: z.Action, _: z.Mods) void {
            var f: k.K = 0;  _ = k.K0(&f, "key", &[_]k.K{k.Ki(s), k.Ki(@enumToInt(a))}, 2);
        }
    }).cb);

    while(!win.shouldClose()) {
        try z.pollEvents();

        const p = win.getUserPointer(WD).?;

        if((p.pws[0] != p.nws[0]) or (p.pws[1] != p.nws[1])) {
            p.sc.?.configure(p.scf, .{.render_attachment=true}, p.nws[0], p.nws[1]);
            p.pws = p.nws;
        }

        {var f: k.K = 0;  _ = k.K0(&f, "tick", &k.Ki(0), 1);}

        l: {
            const TK = k.TK;  const NK = k.NK;
            var f: k.K = 0;  const r = k.K0(&f, "draw", &k.Ki(0), 1);
            if(r==0 or TK(r)!='L' or NK(r)!=2) break :l;

            var pd: [2]k.K = .{0,0};  k.LK(&pd, r);
            const tp = TK(pd[0]);  const td = TK(pd[1]);
            const np = NK(pd[0]);  const nd = NK(pd[1]);
            if(tp!='I' or np%3!=0 or np<3 or np>3*32  or  td!='I' or nd!=480*270) break :l;

            var pal = zs([32][3]c_int);     k.IK(@ptrCast(*c_int, &pal), pd[0]);
            var dots = zs([480*270]c_int);  k.IK(@ptrCast(*c_int, &dots), pd[1]);

            for(dots) |d,i| {
                const pi = @intCast(usize, std.math.max(0,d)) % np;
                px[i] = .{tr(pal[pi][0]), tr(pal[pi][1]), tr(pal[pi][2]), 1};
            }
        }
        {const pxs: []const [4]u8 = px[0..];  qu.writeTexture(&.{.texture=tx}, pxs, &dl, &is);}

        const bbv = p.sc.?.getCurrentTextureView();
        const ca = g.RenderPassColorAttachment {
            .view = bbv,  .resolve_target = null,  .clear_value = zs(g.Color),
            .load_op = .clear,  .store_op = .store,
        };

        const enc = set.dev.createCommandEncoder(null);
        const pass = enc.beginRenderPass(&g.RenderPassEncoder.Descriptor {
            .color_attachments = &.{ca}, .depth_stencil_attachment = null,
        });
        pass.setPipeline(pl);  pass.setBindGroup(0, bg, &.{});
        pass.draw(6, 1, 0, 0);  pass.end();  pass.release();

        var cmd = enc.finish(null);  enc.release();
        qu.submit(&.{cmd});          cmd.release();

        p.sc.?.present();  bbv.release();

        // std.time.sleep(8 * std.time.ns_per_ms);
    }
}

fn pue(_: void, ty: g.ErrorType, msg: [*:0]const u8) void {
    switch (ty) {
        .validation    => prt("gpu: validation error: {s}\n", .{msg}),
        .out_of_memory => prt("gpu: out of memory: {s}\n",    .{msg}),
        .device_lost   => prt("gpu: device lost: {s}\n",      .{msg}),
        .unknown       => prt("gpu: unknown error: {s}\n",    .{msg}),
        else           => unreachable,
    }
}
var pue_cb = g.ErrorCallback.init(void, {}, pue);

const S = struct {ni: g.NativeInstance,  dev: g.Device};

fn setPipeline(dev: g.Device, wd: *WD) g.RenderPipeline {
    const shs = @embedFile("kes.wgsl");
    const shm = dev.createShaderModule(&.{.code = .{.wgsl=shs}});
    const bl = g.BlendState {
        .color = .{.operation=.add, .src_factor=.one, .dst_factor=.one},
        .alpha = .{.operation=.add, .src_factor=.one, .dst_factor=.one},
    };
    const ct = g.ColorTargetState {.format=wd.scf, .blend=&bl, .write_mask=g.ColorWriteMask.all};
    const fs = g.FragmentState {.module=shm, .entry_point="fs", .targets=&.{ct}, .constants=null};

    const bgl = dev.createBindGroupLayout(&.{
        .entries = &[_]g.BindGroupLayout.Entry{
            g.BindGroupLayout.Entry.sampler(0, .{.fragment=true}, .filtering),
            g.BindGroupLayout.Entry.texture(1, .{.fragment=true}, .float, .dimension_2d, false),
        },
    });
    const pla = dev.createPipelineLayout(&g.PipelineLayout.Descriptor {
        .bind_group_layouts = &.{bgl},
    });
    const pl = dev.createRenderPipeline(&g.RenderPipeline.Descriptor {
        .fragment = &fs,  .layout = pla,  .depth_stencil = null,
        .vertex      = .{.module=shm, .entry_point="vs", .buffers=null},
        .multisample = .{.count=1, .mask=0xFFFFFFFF, .alpha_to_coverage_enabled=false},
        .primitive   = .{
            .front_face=.ccw, .cull_mode=.none,
            .topology=.triangle_list, .strip_index_format=.none
        },
    });
    shm.release();

    return pl;
}

fn setWgpu(win: *z.Window, wd: *WD) !S {
    cg.dawnProcSetProcs(cg.machDawnNativeGetProcs());

    const inst = cg.machDawnNativeInstance_init();
    var ni = g.NativeInstance.wrap(cg.machDawnNativeInstance_get(inst).?);

    cg.machDawnNativeInstance_discoverDefaultAdapters(inst);

    const ap = .{.power_preference = .high_performance};
    const ba = switch (ni.interface().waitForAdapter(&ap)) {
        .adapter => |v| v,
        .err => |err| {
            std.debug.print("failed to get adapter: error={} {s}\n", .{err.code, err.message});
            std.process.exit(1);
        },
    };

    const pr = ba.properties;
    prt("found {s} backend on {s} adapter: {s}, {s}\n", .{
        g.Adapter.backendTypeName(pr.backend_type),  g.Adapter.typeName(pr.adapter_type),
        pr.name,  pr.driver_description,
    });

    const dev = switch (ba.waitForDevice(&.{})) {
        .device => |v| v,
        .err => |err| {
            prt("failed to get device: error={} {s}\n", .{err.code, err.message});
            std.process.exit(1);
        },
    };

    dev.setUncapturedErrorCallback(&pue_cb);

    win.setUserPointer(wd);
    {
        const fbs = try win.getFramebufferSize();
        const b = cg.machUtilsCreateBinding(
            @enumToInt(g.Adapter.BackendType.vulkan),
            @ptrCast(*cg.GLFWwindow, win.handle),
            @ptrCast(cg.WGPUDevice, dev.ptr)
        );
        if (b == null) {@panic("failed to create Dawn backend binding");}

        var desc = zs(g.SwapChain.Descriptor);
        desc.implementation = cg.machUtilsBackendBinding_getSwapChainImplementation(b);
        wd.sc = dev.nativeCreateSwapChain(null, &desc);

        wd.scf = @intToEnum(
            g.Texture.Format,
            @intCast(u32, cg.machUtilsBackendBinding_getPreferredSwapChainTextureFormat(b))
        );
        wd.sc.?.configure(wd.scf, .{.render_attachment=true}, fbs.width, fbs.height);
    }

    return S{.ni=ni, .dev=dev};
}
