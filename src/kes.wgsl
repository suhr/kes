@group(0)@binding(0) var sm: sampler;
@group(0)@binding(1) var tx: texture_2d<f32>;

struct VO {@builtin(position) p: vec4<f32>,  @location(0) u: vec2<f32>}

let sv = array<f32, 12>(1.0, 1.0,  -1.0, -1.0,  1.0, -1.0,    1.0, 1.0,  -1.0, 1.0,  -1.0, -1.0);
let su = array<f32, 12>(1.0, 0.0,  0.0, 1.0,  1.0, 1.0,    1.0, 0.0,  0.0, 0.0,  0.0, 1.0);

@stage(vertex) fn vs(@builtin(vertex_index) i: u32) -> VO {
    var o: VO;
    o.p = vec4<f32>(sv[2u*i], sv[2u*i + 1u], 0.0, 1.0);
    o.u = vec2<f32>(su[2u*i], su[2u*i + 1u]);
    return o;
}
@stage(fragment) fn fs(@location(0) uv: vec2<f32>) -> @location(0) vec4<f32> {
    return textureSample(tx, sm, uv);
}
