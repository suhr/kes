{
  description = "K Entertainment System";

  inputs.zig.url = "github:suhr/zig-overlay";
  outputs = {self, zig, nixpkgs}:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      deps = with pkgs; [
        zig.packages.x86_64-linux.master.latest
        pkgconfig libsoundio glfw3 libepoxy xorg.libX11
      ];
    in {
      packages.x86_64-linux.hello = nixpkgs.legacyPackages.x86_64-linux.hello;
      defaultPackage.x86_64-linux = self.packages.x86_64-linux.hello;

      devShell.x86_64-linux =
        with pkgs; mkShell {
          buildInputs = deps;
          shellHook = ''
            export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${
              pkgs.lib.makeLibraryPath [
                vulkan-loader libGL
                xorg.libX11 xorg.libXrandr xorg.libXcursor xorg.libXi
              ]
            }"'';
        };
    };
}
